# Read and load a docker image from dockerhub
FROM library/container-name:container-version

# Arguements passed in from docker-compose.yml file
# if no docker-compose.yml just set a default value then read from CI secret variables
ARG CF_KEY_PAIR_ID=""
ARG CF_PRIVATE_KEY=""

# This ${} is how you read `.env` vars as `ARG` vars in `Dockerfile` then pass them to `ENV` vars
ENV CF_KEY_PAIR_ID=${CF_KEY_PAIR_ID}
ENV CF_PRIVATE_KEY=${CF_PRIVATE_KEY}
ENV PORT 3000

# Set the working directory inside the container
WORKDIR "/app"

# Copy all the files except those in `.dockerignore` into the container 
COPY . /app

# Run some commands, here needs to run `npm install`
RUN npm install

# Expose the container port to outside
EXPOSE 3000

# Run some CMD commands
CMD [ "npm", "start" ]