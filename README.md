#Some useful docs

##How to set up Docker
[Docker tutorial](https://docs.docker.com/get-started/) - my recommendation is to do this whole tutorial on docker website.
* ######Local environment
    Either use built in docker images in DockerHub, or build your own image and push to DockerHub, then pull and use it. However, if you are planing to use few containers in one network, eg : `sever`, `database`, `cache` and etc, write a `docker-compose.yml` file (see an example in this repo), then just run these commands:
    ```
	    docker-compose build
	    docker-compose up -d
	```
    To see the logs (only last 100 lines) and follow them:
    ```
    docker-compose logs -f --tail=100
    ```
* ######Staging and production environment
    To just build the `Dockerfile` to test:
    ```
    docker build -t test/test:test .
    ```
#####ENV variables
Put you env variables in `.env` but dont commit to git repo. Use secret variables in CI tool to set the sensitive variables. You can have another `.env` file to have less sensitive variables for the app if you need. docker compose file read your env variables like this `${YOUR_ENV_VAR}`, to pass them to dockerfile you can pass as `args` then load them back to env var:
```
ENV YOUR_ENV_VAR=${YOUR_ENV_VAR}
```
	
##How to set up CI/CD
[What is CI/CD?](https://blog.gds-gov.tech/that-ci-cd-thing-principles-implementation-tools-aa8e77f9a350)

In most cases you can use interface or create a `.yml` file to configure your pipeline. If you use GitHub [here](https://blog.github.com/2017-11-07-github-welcomes-all-ci-tools/) is a good reference for choosing your CI/CD tool. We use GitLab so we use `gitlab-ci.yml`. The step by step description of our example is written as comments on top of each section of `gitlab-ci.yml` in this repo, here is the overview:
* ######How does it work:
    CI tools build a docker container and run CI/CD commands in it (Please read more online, it might be different in different tools), usually it has some different stages such as `build`, `test`, `deploy` and etc. Some tools only do CI, so they might not have the `deploy` stage.

* ######Stages
    We use 3 stages including `build`, `test` and `deploy`. `Build` is to check the health of the project build, `test` is to run all the automatic test cases, and `deploy` is to roll out to environments such as Staging and Production, this can be automatic or manual. The deployment part is subjective to where and how you are deploying your app to, in this case we are deploying docker containers on AWS ECS service, using aws cli.

* ######Variables 
	To run the scripts, use variables that can be configured instead of hard coding them in the script.
	Some variables are not sensetive so you can set the `.yml' file, and set the sensitive ones in CI tool's secret variables

* ######Templating
	Create templates for different stages, it helps to remove reduncency (Its optional), you can set variables or specify other things in template and use them over and over again.

* ######Before Script 
	Before any stages it runs this `before_script`, specially in senarios that you need to set up some global packages and use them in the entire pipeline.


##How to set up Error and Monitoring Tool
##How to set up API Gateway